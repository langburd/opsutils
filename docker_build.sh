#!/bin/bash

# Build and run in local environment:

TAG=v1.0.0
REGISTRY=docker-registry.io/ops
IMAGE=opsutils

docker build --tag $REGISTRY/$IMAGE:$TAG .
docker push $REGISTRY/$IMAGE:$TAG

docker stop $IMAGE; docker run -d --rm --name $IMAGE $REGISTRY/$IMAGE:$TAG && docker logs -f $IMAGE
