# OPS K8S Utils

## Set of usefull utilies for debug in K8S environment

- htop
- net-tools
- bind-utils
- nano
- wget
- curl
- tcptraceroute
- tcpping

## Build new version (add/change utils etc.)

### Build using GitLab CI

- Push your code
- Add tag, using 'v' + [SEMVER](https://semver.org/) convention - `vX.X.X`

### Build and run in local environment

```bash
TAG=v1.1.0
REGISTRY=docker-registry.io/ops
IMAGE=opsutils

docker build --tag $REGISTRY/$IMAGE:$TAG .
docker push $REGISTRY/$IMAGE:$TAG

docker stop $IMAGE; docker run -d --rm --name $IMAGE $REGISTRY/$IMAGE:$TAG && docker logs -f $IMAGE
```

## Installation in K8S cluster

Add helm repo:

```bash
helm repo add helm https://helm-repo.io/helm
```

Install OPS Utils Helm chart:

```bash
helm upgrade --install opsutils helm/opsutils --version 1.0.3 --namespace ops
```

## Examples of usage

```bash
kubectl exec -it -n ops deployment/opsutils -- bash
kubectl exec -it -n ops deployment/opsutils -- nslookup google.com
kubectl exec -it -n ops deployment/opsutils -- tcpping -x 4 ynet.co.il 443
kubectl exec -it -n ops deployment/opsutils -- curl -I https://ident.me
```
