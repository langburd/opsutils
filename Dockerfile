FROM centos:7

LABEL maintainer="Avi Langburd <avi@langburd.com>"

COPY tcpping /usr/bin/

# hadolint ignore=DL3033
RUN yum makecache fast \
    && yum install htop net-tools bind-utils nano wget curl tcptraceroute bc psmisc -y \
    && chmod 755 /usr/bin/tcpping \
    && yum clean all \
    && rm -rf /var/cache/yum/*

COPY *.sh /

RUN chmod +x /opsutils.sh

CMD ["/opsutils.sh"]
