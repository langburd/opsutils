#!/bin/bash
while (true) ; do
  [[ -z "${HOST_TO_PING}" ]] && HOST='google.com' || HOST="${HOST_TO_PING}"
  [[ -z "${PORT_TO_PING}" ]] && PORT='443' || PORT="${PORT_TO_PING}"
  echo
  echo "=============== Checking connection to $HOST:$PORT ==============="
  /usr/bin/tcpping -d -x 1 "$HOST" "$PORT"
  sleep 5
done
